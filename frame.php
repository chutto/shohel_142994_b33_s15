<!DOCTYPE html>
<html lang="en">
<head>
    <title>Basic Bootstrap Form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assest/BO/css/bootstrap.min.css">
    <script src="assest/BO/js/jquery.min.js"></script>
    <script src="assest/BO/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #1b170a; color: white">
<div class="container">
    <h1 align="center">Ragistration Form by Bootstrap</h1>
    <form>

        <div class="form-group">
            <label for="name">First Name:</label>
            <input type="text" class="form-control" id="usr" placeholder="Enter Your First Name">
        </div>
        <div class="form-group">
            <label for="name">Last Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Your Last Name">
        </div>


        <div class="form-group">
            <label for="Birthday">Birthday:</label>
            <input type="date" class="form-control" id="birthday">
        </div>
        <div class="form-group">
            <label for="Select">Select Your Gender</label>
            <select class="form-control" id="Select">
                <option>Select One</option>
                <option>Male</option>
                <option>Female</option>
                <option>Others</option>
            </select>
        </div>
        <div class="form-group">
            <label for="number">Contact</label>
            <input type="tel" class="form-control" id="number" placeholder="+880">

        </div>
        <div class="form-group">
            <label for="Address">Address</label>
            <input type="text" class="form-control" id="address" placeholder="Address">
        </div>
        <div class="form-group">
            <label for="InputFile">Upload Your Photo</label>
            <input type="file" class="form-control-file" id="InputFile">
            <small id="fileHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Enter Your Email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password">
        </div>
        <button type="submit" class="btn bg-primary btn-block btn-lg">Submit</button>


    </form>

</div>
</body>
